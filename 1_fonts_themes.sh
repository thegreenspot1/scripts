#!/bin/bash

# Update system
sudo pacman -Syu --noconfirm

# List of fonts to install
fonts=(
    ttf-meslo-nerd-font-powerlevel10k
    ttf-sourcecodepro-nerd
    ttf-roboto-mono-nerd
    ttf-ubuntu-font-family
    ttf-jetbrains-mono-nerd
    ttf-font-awesome
)

# List of themes to install
themes=(
    dracula-gtk-theme
    nordic-darker-theme
    papirus-icon-theme
)

# Function to install a package with pacman or paru
install_package() {
    if sudo pacman -S --noconfirm "$1"; then
        echo "$1 installed successfully with pacman."
    else
        echo "$1 not found in official repos, attempting to install with paru..."
        paru -S --noconfirm "$1" && echo "$1 installed successfully with paru."
    fi
}

# Install each font package
for font in "${fonts[@]}"; do
    install_package "$font"
done

# Install each theme package
for theme in "${themes[@]}"; do
    install_package "$theme"
done

echo "All specified fonts and themes have been processed."

