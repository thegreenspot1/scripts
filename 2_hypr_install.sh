#!/bin/bash

# This install script is for a HYPRLAND installation from a "minimal" base ARCH install with no sound installed.
# Durning the installation script from ARCH under "Additional packages" install the following:
#  * git
#  * vim
#  * fish

# After the base install is complete and you have rebooted. Complete the follwing:
#   1) Connect to the internet with "nmtui"
#   2) Change the SHELL to fish using "chsh -s /bin/fish"
#   3) Reboot
#   4) mkdir .local/git
#   5) cd .local/git
#   6) git clone https://gitlab.com/thegreenspot1/config_files.git

# After the below installation script is completed I will need to add my personal files / file structure with my central file system (aka cfs)
# The Script
# ***********************************************************************************************************************

# Update system and install required packages
sudo pacman -Syu --noconfirm

# User
CURRENT_USER=$(logname)

# Install programs
packages=(
    hyprland
    aquamarine
    hyprlang
    hyprcursor
    hyprutils
    hyprgraphics
    hyprwayland-scanner
    hypridle        # hyprland idle for laptops
    hyprlock        # hyprland lock screen
    hyprpaper       # hyprland wallpaper processor
    waybar          # status bar
    wofi            # application launcher
    xdg-desktop-portal-hyprland
    hyprpolkitagent
    hyprland-qt-support
    hyprland-qtutils
    mako
    qt5-wayland
    qt6-wayland
    discord
    blueberry       # bluetooth applications
    alacritty       # termianl
    thunar          # file manager
    alsa-utils      # sound
    alsa-firmware   # sound
    sof-firmware    # sound
    pavucontrol
    brightnessctl   # brightness control
    nwg-look        # gtk settings
    gvfs            # to access network
    gvfs-smb        # to access network
    okular          # pdf viewer
    pdfarranger     # pdf viewer
    neofetch        # info
    keepassxc       # password manager
    htop            # computer information - task manager
    darktable       # complete phot editor
    feh             # simple photo viewer
    swappy          # simple photo editor
    grim            # screenshot
    slurp           # screenshot
    wl-clipboard    # clipboard
    neovim
    fzf
    bat
)

for package in "${packages[@]}"; do
    sudo pacman -S --noconfirm $package
done

# Create symlinks
symlinks=(
    "/home/$CURRENT_USER/.local/git/dotfiles/alacritty/ /home/$CURRENT_USER/.config"
    "/home/$CURRENT_USER/.local/git/dotfiles/gtk-3.0/ /home/$CURRENT_USER/.config"
    "/home/$CURRENT_USER/.local/git/dotfiles/htop/ /home/$CURRENT_USER/.config"
    "/home/$CURRENT_USER/.local/git/dotfiles/hypr/ /home/$CURRENT_USER/.config"
    "/home/$CURRENT_USER/.local/git/dotfiles/neofetch/ /home/$CURRENT_USER/.config"
    "/home/$CURRENT_USER/.local/git/dotfiles/waybar/ /home/$CURRENT_USER/.config"
    "/home/$CURRENT_USER/.local/git/dotfiles/tmux/tmux.conf/ /home/$CURRENT_USER/"
    "/home/$CURRENT_USER/.local/git/dotfiles/vim/.vimrc/ /home/$CURRENT_USER/"
    "/home/$CURRENT_USER/.local/git/dotfiles/nvim/ /home/$CURRENT_USER/.config"
)

for link in "${symlinks[@]}"; do
    src=$(echo $link | cut -d':' -f1)
    dest=$(echo $link | cut -d':' -f2)

    if [ -e "$src" ]; then
        ln -sf $src $dest
        echo "Symlink created: $src -> $dest"
    else
        echo "Source file does not exist: $src"
    fi
done


# Run amixer commands to unmute sound channels
#amixer sset Master unmute
#amixer sset Speaker unmute
#amixer sset Headphone unmute

echo "Installation and configuration completed."


